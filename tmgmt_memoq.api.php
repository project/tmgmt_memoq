<?php

/**
 * @file
 * Hooks provided by the MemoQ translator module.
 */

use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;

/**
 * Allows to alter order before submitting to MemoQ.
 *
 * @param array $order
 *   Order information.
 * @param \Drupal\tmgmt\JobInterface $job
 *   Translation job object.
 */
function hook_tmgmt_memoq_order_info_alter(array &$order, JobInterface $job) {
  $words_count = 0;
  /** @var JobItemInterface $job_item */
  foreach ($job->getItems() as $job_item) {
    $words_count += $job_item->getWordCount();
  }
  // Add number of words to the order name.
  $order['Name'] .= $words_count;
}

/**
 * Allows to alter translation job before submitting to MemoQ.
 *
 * @param array $translation_job
 *   Translation job information.
 * @param \Drupal\tmgmt\JobItemInterface $job_item
 *   Translation job object.
 */
function hook_tmgmt_memoq_job_info_alter(array &$translation_job, JobItemInterface $job_item) {
  // Get the job object.
  $job = $job_item->getJob();
  // Add translation request owner display name to job name.
  $translation_job['Name'] = $job->getOwner()->getDisplayName() . ' ' . $translation_job['Name'];
}
