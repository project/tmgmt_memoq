<?php

/**
 * @file
 * Contains Drupal\tmgmt_memoq\MemoQTranslatorUi.
 */

namespace Drupal\tmgmt_memoq;

use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;

/**
 * MemoQ translator UI.
 */
class MemoQTranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => t('CMS API URL'),
      '#default_value' => $translator->getSetting('api_url'),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('CMS API key'),
      '#default_value' => $translator->getSetting('api_key'),
    ];
    $form['job_name_prefix'] = [
      '#type' => 'textfield',
      '#title' => t('Prefix for the MemoQ order name'),
      '#default_value' => $translator->getSetting('job_name_prefix'),
    ];
    $form += parent::addConnectButton();

    $form['memoq_languages'] = [
      '#type' => 'details',
      '#title' => t('MemoQ language mapping'),
      '#open' => TRUE,
    ];

    $configured_mappings = $translator->getSetting('memoq_languages') ?? [];
    $language_manager = \Drupal::languageManager();
    foreach ($language_manager->getLanguages() as $langcode => $language) {
      $form['memoq_languages'][$langcode] = [
        '#type' => 'select',
        '#required' => $translator->checkAvailable()->getSuccess(),
        '#title' => $language->getName(),
        '#options' => $translator->getPlugin()->getSupportedMemoqLanguages($translator),
        '#default_value' => $configured_mappings[$langcode] ?? NULL,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_memoq\Plugin\tmgmt\Translator\MemoQTranslator $translator_plugin */
    try {
      $translator_plugin = $translator->getPlugin();
      $translator_plugin->setTranslator($translator);

      $translator_plugin->testConnection();
    }
    catch (\Exception $e) {
      $element = $form['plugin_wrapper']['settings']['api_url'] ?? $form['plugin_wrapper']['settings'];
      $form_state->setError($element, $e->getMessage());
    }
  }
  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    /** @var \Drupal\tmgmt_memoq\Plugin\tmgmt\Translator\MemoQTranslator $translator_plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->setTranslator($job->getTranslator());

    $form['deadline'] = [
      '#type' => 'datetime',
      '#title' => t('Deadline'),
      '#default_value' => $job->getSetting('deadline') ? $job->getSetting('deadline') : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    $form = [];

    if ($job->isActive()) {
      $form['actions']['pull'] = [
        '#type' => 'submit',
        '#value' => t('Pull translations'),
        '#submit' => [[$this, 'submitPullTranslations']],
        '#weight' => -10,
      ];
    }

    return $form;
  }

  /**
   * Submit callback to pull translations form MemoQ.
   */
  public function submitPullTranslations(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_memoq\Plugin\tmgmt\Translator\MemoQTranslator $translator_plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->fetchJobs($job);
    tmgmt_write_request_messages($job);
  }

}
