<?php

namespace Drupal\tmgmt_memoq\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\RemoteMapping;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Route controller class for the tmgmt_memoq module.
 */
class MemoQController extends ControllerBase {

  /**
   * Provides a callback function for MemoQ translator.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to handle.
   * @param \Drupal\tmgmt\Entity\Job $tmgmt_job
   *   The tmgmt job.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to return.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function callback(Request $request, Job $tmgmt_job) {
    $content = Json::decode($request->getContent());
    if (empty($content['Payload']['NewStatus'])) {
      throw new NotFoundHttpException();
    }

    if ($content['Payload']['NewStatus'] == 'TranslationReady') {
      $remotes = RemoteMapping::loadByRemoteIdentifier(NULL, $content['Payload']['TranslationJobId']);
      if (empty($remotes)) {
        throw new NotFoundHttpException();
      }

      /** @var \Drupal\tmgmt\Entity\RemoteMapping $remote */
      $remote = array_shift($remotes);
      $job_item = $remote->getJobItem();

      if (!$job_item) {
        throw new NotFoundHttpException();
      }

      /** @var \Drupal\tmgmt_memoq\Plugin\tmgmt\Translator\MemoQTranslator $translator_plugin */
      $translator_plugin = $job_item->getTranslator()->getPlugin();
      $translator_plugin->setTranslator($job_item->getTranslator());
      try {
        $translator_plugin->retrieveTranslation($content['Payload']['TranslationJobId'], $tmgmt_job);
      }
      catch (\Exception $e) {
        $tmgmt_job->addMessage('An error occurred while fetching translations: @error',
          ['@error' => $e->getMessage()]);
      }

      return new Response();
    }
  }

}
