<?php

namespace Drupal\tmgmt_memoq\Plugin\tmgmt\Translator;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorPluginBase;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\AvailableResult;

/**
 * MemoQ translation plugin controller.
 *
 * @TranslatorPlugin(
 *   id = "tmgmt_memoq",
 *   label = @Translation("MemoQ"),
 *   description = @Translation("MemoQ"),
 *   ui = "Drupal\tmgmt_memoq\MemoQTranslatorUi",
 *   logo = "icons/memoq.png",
 * )
 */
class MemoQTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The translator.
   *
   * @var \Drupal\tmgmt\TranslatorInterface
   */
  protected $translator;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The format plugin manager.
   *
   * @var \Drupal\tmgmt_file\Format\FormatManager
   */
  protected object $formatPluginManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Sets a Translator.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   */
  public function setTranslator(TranslatorInterface $translator) {
    $this->translator = $translator;
  }

  /**
   * Constructs a MemoQTranslator object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $tmgmt_file
   *   The tmgmt file plugin manager.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(ClientInterface $client, EntityTypeManagerInterface $entity_type_manager, PluginManagerInterface $tmgmt_file, ModuleHandlerInterface $module_handler, $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->formatPluginManager = $tmgmt_file;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('http_client'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.tmgmt_file.format'),
      $container->get('module_handler'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $this->setTranslator($job->getTranslator());

    try {
      $configured_mappings = $job->getTranslator()->getSetting('memoq_languages');

      if (!isset($configured_mappings[$job->getSourceLangcode()], $configured_mappings[$job->getTargetLangcode()])) {
        throw new TMGMTException('The translator does not support this language pair or is not configured correctly.');
      }

      $remote_source_langcode = $configured_mappings[$job->getSourceLangcode()];
      $remote_target_langcode = $configured_mappings[$job->getTargetLangcode()];

      $order = [
        'Name' => $this->translator->getSetting('job_name_prefix') ? $this->translator->getSetting('job_name_prefix') . $job->label() : $job->label(),
        'CallbackUrl' => Url::fromRoute('tmgmt_memoq.callback', ['tmgmt_job' => $job->id()])->setAbsolute()->toString(),
        'TimeCreated' => date('c'),
        'Status' => 'Created',
      ];

      if ($job->getSetting('deadline')) {
        $order['Deadline'] = $job->getSetting('deadline')->format('c');
      }
      $this->moduleHandler->alter('tmgmt_memoq_order_info', $order, $job);
      $created_order = $this->request('orders', 'POST', $order);

      // TMGMT Job <> MemoQ order: remote identifier 1 is used.
      $data = [
        'tjid' => $job->id(),
        'remote_identifier_1' => $created_order['OrderId'],
      ];

      $remote_mapping = RemoteMapping::create($data);
      $remote_mapping->save();

      /** @var JobItemInterface $job_item */
      foreach ($job->getItems() as $job_item) {

        $translation_job = [
          'Name' => $job_item->label(),
          'Url' => $job_item->getSourceUrl()->setAbsolute()->toString(),
          'SourceLang' => $remote_source_langcode,
          'TargetLang' => $remote_target_langcode,
          'FileType' => 'xliff',
        ];
        $this->moduleHandler->alter('tmgmt_memoq_job_info', $translation_job, $job_item);
        $conditions = ['tjiid' => ['value' => $job_item->id()]];
        $xliff = $this->getXliffFormat()->export($job, $conditions);
        $filename = "JobID_{$job->id()}_JobItemID_{$job_item->id()}_{$remote_source_langcode}_{$remote_target_langcode}.xliff";

        $file = [
          'name' => 'file',
          'filename' => $filename,
          'contents' => gzencode($xliff),
          'headers' => [
            'Content-Type' => 'application/gzip',
          ],
        ];
        $created_translation_job = $this->request('orders/' . $created_order['OrderId'] . '/jobs', 'POST', ['translationJob' => Json::encode($translation_job)], [$file]);

        // TMGMT Job item <> MemoQ job: remote identifier 2 is used.
        $job_item->addRemoteMapping(NULL, NULL, ['remote_identifier_2' => $created_translation_job['TranslationJobId']]);
        $job_item->addMessage('MemoQ Job %project_id created.', [
          '%project_id' => $created_translation_job['TranslationJobId'],
        ]);
      }

      // When all job items have been added, change the status to 'committed'
      $this->request('orders/' . $created_order['OrderId'], 'PATCH', ['content' => [
        'NewStatus' => 'Committed',
      ]]);
      $job->submitted('Job has been successfully submitted for translation as order %order_id.', ['%order_id' => $created_order['OrderId']]);
    }
    catch (TMGMTException $e) {
      \Drupal::logger('tmgmt_memoq')->error('Job has been rejected with following error: @error',
        ['@error' => $e->getMessage()]);
      $job->rejected('Job has been rejected with following error: @error',
        ['@error' => $e->getMessage()], 'error');
    }
  }

  /**
   * Does a request to MemoQ services.
   *
   * @param string $path
   *   Resource path.
   * @param string $method
   *   HTTP method (GET, POST...)
   * @param array $params
   *   For GET, query parameters. For POST, either an array to send as JSON or
   *   when files are provided, an array of multipart name => content parts.
   * @param array $files
   *   An array of multipart parts with keys name, contents and filename.
   * @param bool $return_raw
   *   Do not decode the return data. Just return whatever received. This is
   *   useful when receiving translation files.
   *
   * @return array|string
   *   Response array from MemoQ.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request($path, $method = 'GET', $params = [], $files = [], $return_raw = FALSE) {
    $options = [];
    if (!$this->translator) {
      throw new TMGMTException('There is no translator entity.');
    }

    $url = $this->translator->getSetting('api_url') . '/' . $path;

    try {
      $options['headers'] = [
        'Authorization' => 'CMSGATEWAY-API ' . $this->translator->getSetting('api_key'),
        'Accept' => 'application/json',
      ];

      if ($method == 'GET') {
        $options['query'] = $params;
      }
      elseif ($method == 'POST') {
        if ($files) {
          foreach ($params as $form_data_name => $contents) {
            $options['multipart'][] = [
              'name' => $form_data_name,
              'contents' => $contents,
            ];
          }
          $options['multipart'] = \array_merge($options['multipart'], $files);
        }
        else {
          $options['headers']['Content-Type'] = 'application/json';
          $options['json'] = $params;
        }
      }
      elseif ($method == 'PATCH') {
        if (!empty($params['content'])) {
          $options['form_params'] = $params['content'];
        }
      }

      $response = $this->client->request($method, $url, $options);

    }
    catch (BadResponseException $e) {
      $response = $e->getResponse();
      throw new TMGMTException('Unable to connect to MemoQ service due to following error: @error', ['@error' => $response->getReasonPhrase()], $response->getStatusCode());
    }

    // If we are expecting a download, just return received data.
    $received_data = $response->getBody()->getContents();

    if ($return_raw) {
      return $received_data;
    }

    $received_data = json_decode($received_data, TRUE);

    if (!empty($received_data['ErrorCode'])) {
      throw new TMGMTException('MemoQ service returned validation error: #%code %error',
        [
          '%code' => $received_data['ErrorCode'],
          '%error' => $received_data['Message'],
        ]);
    }
    return $received_data;
  }

  /**
   * Get supported languages from Memoq.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   The translator.
   *
   * @return array
   *   Array of languages.
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getSupportedMemoqLanguages(TranslatorInterface $translator) {
    try {
      $this->setTranslator($translator);
      $supported_languages = $this->request('languages');

      // Parse languages.
      foreach ($supported_languages as $language) {
        $this->supportedRemoteLanguages[$language['IsoCode3Letter']] = $language['IsoCode3Letter'];
      }
    }
    catch (\Exception $e) {
      return [];
    }
    return $this->supportedRemoteLanguages;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($translator->getSetting('api_key') && $translator->getSetting('api_url')) {
      return AvailableResult::yes();
    }
    elseif (!extension_loaded('zlib')) {
      AvailableResult::no(t('The php extension zlib is missing. Please contact the system administrator.'));
    }


    return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }

  /**
   * Returns the list of account details.
   *
   * @return array
   *   The list of account details.
   */
  public function testConnection() {
    return $this->request('client');
  }

  /**
   * Receives and stores a translation returned by MemoQ.
   *
   * @param int $memoq_job_id
   *   The memoq job id.
   *
   * @return string
   *   The xliff file contents.
   */
  public function retrieveTranslation(int $memoq_job_id, Job $job) {
    $memoq_job_data = $this->request('jobs/' . $memoq_job_id . '/translation', 'GET', [], [], TRUE);

    $translated_data = $this->getXliffFormat()->import(gzdecode($memoq_job_data), FALSE);
    $job->addTranslatedData($translated_data);

    return $memoq_job_data;
  }

  /**
   * Fetches translations for job items of a given job.
   *
   * @param Job $job
   *   A job containing job items that translations will be fetched for.
   */
  public function fetchJobs(Job $job) {
    // Search for placeholder item.
    $this->setTranslator($job->getTranslator());
    $translated = 0;
    $not_translated = 0;

    // Memoq orders - tmgmt jobs remote mappings have remote_identifier_1
    $remote_storage = $this->entityTypeManager->getStorage('tmgmt_remote');
    $query = $remote_storage->getQuery();
    $query->accessCheck(TRUE);
    $query
      ->condition('tjid', $job->id())
      ->notExists('remote_identifier_2');
    $trids = $query->execute();

    if (empty($trids)) {
      $job->addMessage('There are no remote mappings for this job. Contact the system administrator.');
      return;
    }
    else {
      $remotes = $remote_storage->loadMultiple($trids);
    }

    $remote_job = array_shift($remotes);

    try {
      $memoq_jobs = $this->request('orders/' . $remote_job->getRemoteIdentifier1() . '/jobs');
      foreach ($memoq_jobs as $memoq_job) {

        if ($memoq_job['Status'] == 'TranslationReady') {
          $this->retrieveTranslation($memoq_job['TranslationJobId'], $job);
          $translated++;
        }
        else {
          $not_translated++;
        }
      }
    }
    catch (TMGMTException $e) {
      if ($e->getCode() == 409) {
        $not_translated++;
      }

      $job->addMessage('Could not pull translation resources.', [], 'error');
    }

    if (empty($not_translated)) {
      $job->addMessage('Fetched translations for @translated job items.', ['@translated' => $translated]);
    }
    elseif (empty($translated)) {
      $job->addMessage('None of the translation jobs are translated.');
    }
    else {
      $job->addMessage('Fetched translations for @translated job items, @not_translated are not translated yet.', ['@translated' => $translated, '@not_translated' => $not_translated]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'xliff_processing' => TRUE,
    ];
  }

  /**
   * Get the xlf format plugin instance.
   *
   * @return \Drupal\tmgmt_file\Plugin\tmgmt_file\Format\Xliff
   *   The plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getXliffFormat() {
    return $this->formatPluginManager->createInstance('xlf');
  }

}
